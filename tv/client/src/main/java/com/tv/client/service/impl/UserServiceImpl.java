package com.tv.client.service.impl;

import com.tv.client.dao.UserDao;
import com.tv.client.entity.User;
import com.tv.client.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao userDao;
    public int saveUser(User user) {
        userDao.save(user);
        return 1;
    }
}
