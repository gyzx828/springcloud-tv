package com.tv.client.web;

import java.io.Serializable;

/**
 * Project Name  iov-parent
 * File Name     BaseResponse
 * Package Name  com.mingnuo.common.web
 * Date          2020/7/13 上午10:49
 * Project Desc
 * Copyright (c) 2020, NANTONG MINGNUO Corp. All Rights Reserved.
 */
public class BaseResponse implements Serializable {
    // 请求状态
    boolean status;

    // 请求回应编码
    String  code;

    // 请求结果文字表述
    String  message;

    // 回应会话标识
    String  session;

    // 实际对象载体
    Object  data;

    // 回应时服务端的时间标识
    long    timestamp = System.currentTimeMillis();

    public static BaseResponse success () {
        BaseResponse response = new BaseResponse();
        response.setStatus(true);
        response.setCode("200");
        response.setMessage("操作成功");
        return response;
    }

    public static BaseResponse success (String message) {
        BaseResponse response = new BaseResponse();
        response.setStatus(true);
        response.setCode("200");
        response.setMessage(message);
        return response;
    }


    public static BaseResponse fail (String errMessage) {
        BaseResponse response = new BaseResponse();
        response.setStatus(false);
        response.setMessage(errMessage);
        return response;
    }

    public static BaseResponse fail (String code, String errMessage) {
        BaseResponse response = new BaseResponse();
        response.setStatus(false);
        response.setCode(code);
        response.setMessage(errMessage);
        return response;
    }

    public boolean isStatus() {
        return status;
    }

    public BaseResponse setStatus(boolean status) {
        this.status = status;
        return this;
    }

    public String getCode() {
        return code;
    }

    public BaseResponse setCode(String code) {
        this.code = code;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public BaseResponse setMessage(String message) {
        this.message = message;
        return this;
    }

    public String getSession() {
        return session;
    }

    public BaseResponse setSession(String session) {
        this.session = session;
        return this;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public BaseResponse setTimestamp(long timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public Object getData() {
        return data;
    }

    public BaseResponse setData(Object data) {
        this.data = data;
        return this;
    }
}
