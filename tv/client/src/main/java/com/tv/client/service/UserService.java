package com.tv.client.service;

import com.tv.client.entity.User;

public interface UserService {
    int saveUser(User user);
}
