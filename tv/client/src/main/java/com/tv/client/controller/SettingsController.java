package com.tv.client.controller;

import com.tv.client.entity.User;
import com.tv.client.service.UserService;
import com.tv.client.web.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@EnableEurekaClient
@Controller
@RequestMapping("/setting")
public class SettingsController {
    @Value("${server.port}")
    String port;
    @Autowired
    UserService userService;
    @ResponseBody
    @PostMapping("/save")
    public BaseResponse home(@RequestBody User user){
        user.setCreateBy("Simon");
        userService.saveUser(user);
        return BaseResponse.success();
    }


}
